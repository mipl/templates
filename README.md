# Templates

These are a set of useful recipes that can be used to build your projects easily.

## Docker: `.docker-build.yml`

To build the images to use your code and to deploy it automatically to our [MIPL Docker Hub](https://hub.docker.com/u/mipl).

The template builds a Dockerfile located at `DOCKERFILE` local path and tags it using the pattern `mipl/<name>` where `<name>` is in `IMAGE_NAME`.  These variables default to the project name, as shown in the excerpt below.
```yaml
variables:
  IMAGE_NAME: $CI_PROJECT_NAME
  DOCKERFILE: "docker/$CI_PROJECT_NAME.Dockerfile"
```

To use this template you need to `include` it in your local `.gitlab-ci.yml` and set the image name and the Dockerfile path, as follows:
```yaml
include:
  - project: 'mipl/templates'
    file: '.docker-build.yml'

variables:
  IMAGE_NAME: "awesome-project"
  DOCKERFILE: "docker/project-file.Dockerfile"
```

Note that the Dockerfile and the image name may be different, and that the folder doesn't require to be `docker`.  However, I **strongly** advise you to place your Dockerfiles in a `docker` folder to keep your repository organized.
